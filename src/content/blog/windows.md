---
title: "Sistemul zilei și al vieții"
description: "De ce windowsul este cel mai bun sistem de operare din sec XX"
pubDate: "1 april 2024"
heroImage: "/src/assets/coperte/apu-windowz.png"
badge: "not a joke"
---

Eu fiind adept al tehnologii, deseori sum martor a unor lupte virtuale între adepții diferitor sisteme de operare.
Mă șochează aceste controverse, că doar este evident care sistem este mai bun.

<!-- TODO: fotca măibi -->

Și aici sunt doar cîteva argumente de ce windows domină pe dreptate inima și computadoarele tuturor.

## Factor filozofic

![filosofie prin fereastră](https://i.pinimg.com/originals/89/70/09/8970097725d6434a409d044784438713.jpg) 

Windows este cel mai eficient sistem;
timpul petrecut în așteptare te îndeamnă să gîndești asupra 
cursului vieții,
să-ți revezi scopul vieții tale
și să pui la dezbatere existența în sine...
și în final, cînd navigatorul se deschide, tu înțelegi că viața nu-și are rostul fără windows.

## Securitatea

![security](https://i.ytimg.com/vi/eDG26vCoQu4/maxresdefault.jpg) 


Dupa cum zise Bill Porți, creatorul adevăratului sistem de operare:

> Într-o lume protejată cu pereți ai nevoie de uși, în special de cele din spate

Windowsul este un sistem cu sursă închisă, deci nici un haker în gluga lui suspectă nu-l v-a putea exploata.
În plus MicroSoft P.P., compania din spatele windowsului, are multe miliarde de dolari la dispoziție,
datorită MacroPrețului pe care trebuie să-l plătești pentru sistem.
Cu aceste resurse ei își pot permite să angajeze cei mai de seamă ingineri de securitate,
ca să creeze un sistem impenetrabil ca plumbul.

