---
title: "Babă neagră"
description: "O rețetă rapidă de babă neagră, un deliciu a culinarei Moldovenești."
pubDate: "21 aprilie 2023"
heroImage: "/src/assets/coperte/baba_negra.jpg"
---

## Ingrediente
- 300 g făină
- 300 g zahăr
- 200 ml ulei
- 500 ml lapte
- 250 ml chefir
- 1 lingură de sodă alimentară
- 1 lingură de coniac

## Rețeta
Într-un vas de copt, destul de mare, punem cele `300 g` de grîu peste care adăugăm
`200 ml` de ulei. Amestecăm totul pînă la obținerea unei mase uniforme,
acest pas este important pentru prevenirea formării boțurilor.

În masa uniformizată se adaugă `250 ml` de lapte, după care se amestecă
pînă la uniformizare, după care se adaugă restul laptelui.

Într-un vas separat, mai micuț, se adaugă `250 ml` de chefir, o linguriță
de sodă alimentară și una de coniac. Se amestecă totul bine, după ce
se adaugă în vasul mare.

Punem vasul cu masa obținută în cuptorul preîncălzit la `220°C`, unde se va
menține timp de o oră. După se va scădea temperatura la `180°C` și se va mai
menține timp de o oră.

Poftă bună!! 😋
