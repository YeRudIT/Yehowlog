---
title: "Lumina Prin istorie"
description: "Evoluția concepțiilor de lumină. Proiect școlar."
pubDate: "13 november 2023"
heroImage: "/src/assets/coperte/istoria-luminii.png"
---

E evident că lumina are o importanță primară pentru existența, și
dezvoltarea ființelor umane. Toate invențiile și progresele omenești ar
fi fost imposibile fără capacitatea de a percepe lumina.
Cu toate că lumina este peste tot pe parcursul zilii și vieții omenești, 
un răspuns asupra structurii și compoziției acesteia
e greu de găsit.
În decurs de milenii savanții propuneau diferite concepții, deseori contradictorii,
care să permită înțelegerea mai profundă a lumii în care trăim.


## Epoca Antică

În Grecia antică era populară idea că, din ochii este emanată lumina. Dar
totuși era pusă la dubii. Find că de ar fi fost lumina emanată din ochi,
omul ar fi putut vedea și noapte.

**Lucretius** [^lucretius] scria în lucrarea "Despre Natura Lucrurilor [^natura_luc]":

![lucretius](https://alchetron.com/cdn/lucretius-8e69c176-ac2b-4c33-9444-f9aaab36189-resize-750.jpg) 

> Lumina și căldura solară; acestea sunt compuse din particule invizibile
(atomi) minuscule care,  atunci când sunt împinși, nu pierd timp în a
trage chiar peste interspațiul aerului în direcția dată de împingere.

Cu toate că punctul de vedere are o acuratețe remarcabilă,
viziunile lui Lucretius nu erau acceptate în general.


Mult pînă la Lucretius, **Euclid** [^Euclid] (anii 300 î.HR.) studiînd
proprietățile luminii a depistat că ea se deplasează în linii drepte.
El a descris legile reflecție și le-a studiat matematic.  Acestea sunt
utilizate pînă în zilele de astăzi. [^daleco]

![Euclid](https://live.staticflickr.com/63/204022223_7db3640c7f.jpg)

## Epoca Medievală

![Alhazen](https://upload.wikimedia.org/wikipedia/commons/f/f4/Hazan.png) 

Cea mai mare avansare în perioada medievală, cu privire la concepția luminii,
a făcuto **Ibn Al-Haitam** [^Haitan] (anii 1000 d.Hr.), unul din
cei mai importanți savanți ai epocii medievale.  El a demonstrat
că capacitatea de a vedea este datorată de intrarea luminii în
ochi din surse externe. 

![ochi](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Alhazen1652.png/170px-Alhazen1652.png) 

Al-Haitam își dădea seama că lumina are o viteză limită, și că
refracția are loc din cauza diferenței de viteză în diferite medii. [^daleco] 


## Epoca Modernă


Prima dată viteza luminii a fost calculată de astronomul danez **Ole Rømer** [^ole]. 

![Romer](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/R%C3%B8mer%2C_Ole_%28ur_Ber%C3%B8mte_danske_maend%29.jpg/220px-R%C3%B8mer%2C_Ole_%28ur_Ber%C3%B8mte_danske_maend%29.jpg) 

Studiind mișcarea sateliților Jupiterului, a observat niște iregularități. 
Spre exemplu Io, unul din satelite, răsare mai tîrziu cînd pămîntul se depărtează de Jupiter.
Savantul a atribuit aceasta întîrziere, cu timpul adițional de
care lumina are nevoi pentru a ajunge la pămînt.
El a calculat cu ce viteză lumina trece prin orbita pămîntului. [^ole]

![cum a calculat romer](https://www.rinconeducativo.org/sites/default/files/romer_ole_experimento.jpg) 


În ce privește natura luminii, erau două teorii predominante:

+ Corpusculară: Lumina formată dintr-un flux de particule mici.  Suportată de renumitul savant **Isaac Newton** [^nevvton]

![Newton](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Portrait_of_Sir_Isaac_Newton%2C_1689_%28brightened%29.jpg/220px-Portrait_of_Sir_Isaac_Newton%2C_1689_%28brightened%29.jpg) 

+ Ondulară: Lumina formată din unde ce parcurg prin eter [^dex_eter]. Suportată de savantul olandez **Christiaan Huygens** [^cris]

![christiaan](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Christiaan_Huygens-painting.jpeg/220px-Christiaan_Huygens-painting.jpeg) 


Chiar dacă ambele teorii sunt contradictorii, ele nu se excludeau una pe alta.
Teoria corpusculară era mai acceptată între savanți din cauza
autorității lui Newton în lumea științifică.

Fenomenul luminii pute fi interpretat de fiecare în felul lui. 
Spre exemplu refracția:
<!-- nice image about reflaction of waves -->

Huygens considera că fenomenul este datorat din cauza că viteza
undelor de lumină scade la intrarea în alt mediu.
Pe cînd Newton considera că lumina își schimbă unghiul din cauza că 
viteza particulelor crește la intrarea în alt mediu.

Cu toate că viteza a luminii era cunoscută pe timpul dezbaterii,
demonstrarea uneia dintre teorii calculînd diferența de viteză între medii era
practic imposibilă pe timpurile celea.
De abia în anii 1850 s-a demonstrat că viteza a luminii este mai mică în apă.
Cea ce demonstra că lumina are un caracter ondulatoriu.  [^3]


În secolul a XIX-lea, **James Maxwell** [^max] a format teoria existenței undelor electro magnetice.
Și a ajuns la concluzia că lumina se propagă în spațiu sub formă de unde electromagnetice.

![Maxwell](https://upload.wikimedia.org/wikipedia/en/thumb/4/42/Db_James_Clerk_Maxwell_in_his_40s_-2-7.jpg/220px-Db_James_Clerk_Maxwell_in_his_40s_-2-7.jpg) 

S-a constatat că undele de lumină sunt cuprinse într-un spectru încust,
cu lungimi de la 7.8*10<sup>-7</sup> m (4*10<sup>14</sup> Hz) -
lumina roșie, pînă la 3.9*10<sup>-7</sup> m (7.7*10<sup>14</sup>
Hz) - lumina violetă. [^fizicaXII]
![spectrul-undelor](https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/EM_Spectrum_Properties_%28Amplitude_Corrected%2C_Bitmap%29.png/330px-EM_Spectrum_Properties_%28Amplitude_Corrected%2C_Bitmap%29.png) 


![Heinrich Hertz](https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Heinrich_Rudolf_Hertz.jpg/220px-Heinrich_Rudolf_Hertz.jpg) 

Prin anii 1885, **Heinrich Hertz** [^hz] a descoperit că o placă metalică
fiind iluminată de lumină ultravioletă, emite electricitate. Pe cînd
iluminîndo cu lumină roșie nu emită electricitate.
Efectul dat, numit efect **fotoelectric**, nu putea fi explicat
de natura lumina fiind numai o undă ce transmite energie.

![efect fotoelectric](https://www.nsta.org/sites/default/files/2020-02/TST_12_Figure1.jpg) 

## Epoca Contemporană

![Einstein](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Einstein_1921_by_F_Schmutzer_-_restoration.jpg/220px-Einstein_1921_by_F_Schmutzer_-_restoration.jpg) 

Inspirat de **Max Planck** [^planck], **Albert Einstein** [^ein] a fost
recunoscător de ideile lui Newton, a propus teoria că lumina este
creată din particule mici *quante* mai tîrziu numite *fotoni*.  A căror
energie depinde de frecvența undei asociate.


Astfel, în dependență
de interacțiunea luminii cu materia, lumina se poate manifesta atît ca
particule cît și ca undă. [^3]


## Concluzie

![tadam](/elecromagnetic-cat.png) 

Pînă și în zilele noastre conceptul de lumină e complicat de-l definit.
Cu toate că nu putem cu siguranță numi natura luminii și structura ei,
efortul depus de milenii spre definirea acesteia ne-a luminat calea 
spre înțelegere a lumii înconjurătoare. 

____

[^wiki]: Wikipedia

[^daleco]: [Lumina prin timp: De la Grecia antică pînă la Maxwell](https://web.archive.org/web/20170319180859/http://www-groups.dcs.st-and.ac.uk/history/HistTopics/Light_1.html)

[^be_smart]: [be smart](https://invidious.flokinet.to/watch?v=ak7GB74Qlug)


[^fizicaXII]: [manualul de fizică clasa a XII](https://drive.google.com/file/d/1JO2xARLqCbYgTNGZ2jmYTDY0lnAk4s_l/view)


[^Lucretius]: [Lucretius: Poet și filosof roman](https://ro.wikipedia.org/wiki/Titus_Lucretius_Carus?lang=ro)

[^Euclid]: [Euclid: Matimatician grec, considerat "Tatăl Geometriei"](https://ro.wikipedia.org/wiki/Euclid)

[^Haitan]: [Alhazen: Savant arab](https://ro.wikipedia.org/wiki/Alhazen)

[^ole]: [Ole Romer: Astronom danez](https://ro.wikipedia.org/wiki/Ole_Rømer)

[^nevvton]: [Isaac Newton: Renumitul savant](https://ro.wikipedia.org/wiki/Isaac_Newton)

[^cris]: [Christiaan Huygens: Fizician Danez](https://ro.wikipedia.org/wiki/Christiaan_Huygens)

[^natura_luc]: [De rerum natura](https://en.wikipedia.org/wiki/De_rerum_natura)

[^max]: [James Maxwell](https://ro.wikipedia.org/wiki/James_Clerk_Maxwell)

[^hz]: [Heinrich Hertz](https://ro.wikipedia.org/wiki/Heinrich_Hertz)

[^ein]: [Einstein](https://ro.wikipedia.org/wiki/Albert_Einstein)

[^plank]: [Max Planck](https://ro.wikipedia.org/wiki/Max_Planck)

[^3]: [Synchrotron SOLEIL](https://invidious.flokinet.to/watch?v=OLCqaWaV6jA) 

[^dex_eter]: 

    > eter, eteri: substantiv masculin
    >
    >  Substanță ipotetică (a cărei existență nu este admisă de fizica
    >  modernă) având proprietăți fizice contradictorii, care ar umple întregul
    >  spațiu și ale cărei oscilații ar constitui undele electromagnetice.<br>
    [dexonline](dexonline.ro)
