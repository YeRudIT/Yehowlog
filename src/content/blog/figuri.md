---
title: "Figuri geometrice de rotație"
description: "Cazuri de utilizare a figurilor geometric de rotații, pentru proiect la matematică"
pubDate: "14 april 2024"
badge: "NOU"
heroImage: "/src/assets/coperte/matan-thumb.png"
includeMatex: true
---

## Cilindru

Chiar într-un [proiect recent la geografi](http://localhost:3000/blog/turbine#putere) am avut nevoie să utilizez cilindru pentru calcularea energii primite dintr-o turbină eoliană.

Știm că turbina transformă energia vîntului în cea electrică, deci calculînd
energia vîntului putem calcula cantitatea de electricitate generată de turbină.
Noi știm că energia generată nu poate depăși energia vîntului,
aceasta ar însemna că energia apare din nimic, ceea ce violează
[legile conservării
energiei](https://wiki.froth.zone/wiki/Legea_conserv%C4%83rii_energiei?lang=ro).(Legile date nu sunt ca niște promisiune spuse de mine. Sunt legi cărora li se supune tot ce există.)

Energia(cinetică) a vîntului poate fi calculată cu relația  <ma-tex>E\_c=\frac{m\cdot v^2}{2}</ma-tex>,
unde __m__ este masa aerului care antrenează elicile turbinei, iar **v** este viteza masei de aer.
Masa de aer poate fi calculată dacă îmi vom imagina aerul ca un cilindru, baza fiind cercul descris de elicea efectînd o revoluție,
iar înalțimea fiind distanța parcursă de vînt într-un timp **t** dat, exemplificat în imaginea de mai jos.

![energia cinetica](/img/turbine/calcularea-energiei.png) 

Suprafața lovită de aer v-a fi:

<ma-tex>
\begin{aligned}
S&=\pi\cdot r^2
\end{aligned}
</ma-tex>


Volumul de aer care v-a lovi turbina, se v-a calcula dupa relația de calcularea volumului unui cilindru:

<ma-tex>
\begin{aligned}
V&=S\cdot d
\end{aligned}
</ma-tex>

Unde distanța este <ma-tex> d = v \cdot t</ma-tex>:
<!-- adică volumul de aer ce va trece prin turbină cu viteza de <ma-tex>2m/s</ma-tex> timp de <ma-tex>1s</ma-tex>. -->

<ma-tex>
\begin{aligned}
V&=\pi \cdot r^2 \cdot v \cdot t
\end{aligned}
</ma-tex>

Masa va fi produsul dintre volumul și densitatea aerului (<ma-tex>\rho=1.225 \frac{kg}{m^3}</ma-tex>)

<ma-tex>
\begin{aligned}
m&=V\cdot \rho\\
m&=\pi \cdot r^2 \cdot v \cdot t \cdot \rho
\end{aligned}
</ma-tex>

Din acestea deducem relația:

<ma-tex class="text-2xl">E\_c=\frac{\pi \cdot r^2 \cdot t \cdot \rho \cdot v^3}{2}</ma-tex>

Unde pentru simplitate <ma-tex>t = 1s</ma-tex>

<ma-tex class="text-4xl inline-block w-full text-center">E\_c=\frac{\pi \cdot r^2 \cdot \rho \cdot v^3}{2}</ma-tex>


Drept într-o lume perfectă, unde nu-s de loc pierderi de energie și unde eu finisez la timp proiectele,
o turbină eoliană cu elicea de <ma-tex>1m</ma-tex> și la viteza de <ma-tex>2m/s</ma-tex> v-a produce <ma-tex>~15,4J</ma-tex> timp de o secundă, sau <ma-tex>~15.4W</ma-tex>.

## Sfera

Din cauza că pămîntul este rotund, obiectele de aceiași lungime, pe același meridian, perpendiculare pe pămînt,
vor avea lungimi de umbre diferite.

![căderea luminii](https://searx.be/image_proxy?url=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fd%2Fd1%2FEratosthenes_experiment.png%2F151px-Eratosthenes_experiment.png&h=73de105552639f04320b08d3aed457bd9847a392b500b2b56e9cf88b11cfd84b) 

Cu ajutorul acestui fenomen se poate de calculat circumferința pămîntului(pe meridianul dat).
Defapt aceasta și a făcut-o matimaticianul grec **Eratostene**[^Eratostene].

<img src="https://wiki.froth.zone/media/wikipedia/commons/thumb/e/ee/Eratosthenes_Teaching_in_Alexandria_%28Bernardo_Strozzi%2C_Montreal%29.jpg/750px-Eratosthenes_Teaching_in_Alexandria_%28Bernardo_Strozzi%2C_Montreal%29.jpg" alt="Eratistene" class="w-1/2"> </img>

La momentul solstițiului de vară (21 iunie), la ora *12:00*, cînd razele solare cădeau perpendicular pe localitatea **Assuan**.
La aceeași dată și oră, în orașul **Alexandria**, situat aproximativ pe același meridian ca și Assuan (diferență de <ma-tex>2 \degree</ma-tex>), umbra lăsată de un turn reprezenta <ma-tex class="text-2xl">\frac{1}{50}</ma-tex> din circumferința unui cerc. Aceasta corespunde unui unghi de aproximativ <ma-tex>7 \degree 12'</ma-tex>.
Distanța dintre acestea localități era de aproximativ `800km` (`5000 stadii`).
Astfel, un cerc mare al sferei cu care era aproximat Pământul era de cca <ma-tex>50 \cdot 5.000 = 250.000stadii \approx 39.690km</ma-tex>.[^CP]

![experiment](https://wiki.froth.zone/media/wikipedia/commons/thumb/1/1f/Eratosthenes_measure_of_Earth_circumference.svg/512px-Eratosthenes_measure_of_Earth_circumference.svg.png) 

<iframe id='ivplayer' src='https://inv.nadeko.net/embed/YaPa4esJJx4' class="m-auto w-full h-96"></iframe>
<div class="text-center">În acest videou autorul calculează circumferința pămîntului cu aceiași metodă</div>

## Con

Vom calcula suprafața de hîrtie necesară pentru a creea un con.

<img src="https://i.kym-cdn.com/photos/images/newsfeed/001/225/475/4eb.jpeg" alt="matan" class="w-2/3"></img> 

Un motan poate avea circumferința capului de aproximativ `23-28cm`. Desigur în dependență de specie.
Vom creea un con pentru un motan cu circumferința capului de `25cm`.

<img src="/img/matan-cil.png" alt="matan" class="w-2/3"></img> 

Din relația pentru circumferința cercului <ma-tex>L\_c = 2\cdot \pi \cdot r </ma-tex>, putem afla raza

<ma-tex class="text-2xl">r = \frac{L\_c}{2\cdot \pi} = \frac{25cm}{2\cdot \pi} = 3.66cm </ma-tex>

Înălțimea o putem lua după dorință. În cazul meu doresc conul să fie de `10cm`.
Pentru a afla aria laterală a conului avem nevoie și de generatoare lui.
Acum că știm înălțimea și raza o putem afla prin teoreama lui Pitagora.


<img src="/img/matan-gen.png" alt="matan" class="w-2/3"></img> 

<ma-tex>
G = \sqrt{H^2 + r^2} = \sqrt{10^2 + 3.66^2} = 10.64 (cm)
</ma-tex>

De acum putem simplu afla aria laterală a conului.

<ma-tex>
A_l = \pi \cdot r \cdot G = \pi \cdot 3.66 \cdot 10.64 = 122.34 (cm^2)
</ma-tex>

Trebuie să luăm în considerare și restul de hîrtie care-l vom utiliza pentru lipirea conului

<ma-tex>
A_r = d \cdot G = 4 \cdot 10.64 = 42.56 (cm^2)
</ma-tex>

<ma-tex>
A_t = A_r + A_l \approx 165 (cm^2)

</ma-tex>


---
[^Eratostene]: [Eratosten](https://ro.wikipedia.org/wiki/Eratostene?lang=ro)
[^CP]: [Circumferința pămîntului](https://wiki.froth.zone/wiki/Eratostene?lang=ro#Activitatea)
