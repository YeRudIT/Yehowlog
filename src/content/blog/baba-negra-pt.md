---
title: "Babă neagră"
description: "Uma receita de 'Babă neagră', uma delicia da culinária Moldava."
pubDate: "21 aprilie 2023"
heroImage: "/src/assets/coperte/baba_negra.jpg"
---

## Ingredientes
- 300 g farinha
- 300 g açúcar
- 200 ml óleo
- 500 ml leite
- 250 ml leite fermentado
- 1 colher de sódio alimentário
- 1 colher de conhaque

## Receita
Num vaso para cozinhar colocamos `300 g` de farinha e `200 ml` de óleo, e mexemos
ate a massa ficar uniforme. Isso e um passo muito importante para não criar bolinhos
de farinha.

Na massa colocamos `250 ml` de leite, e mexemos ate a massa ficar uniforme, depois
colocamos o resto de leite.

Em um vaso separado, mais pequeno, colocamos `250 ml` de leite fermentado, uma colher
de sódio alimentário e uma colher de conhaque. Mexemos todo bem, e jogamos no vaso
mais grande, onde mexemos mexemos mexemos.

Agora colocamos a massa pronta no forno aquecido ate `220°C` onde deixamos cozinhar
uma hora. Depois baixamos a temperatura ate `150°C` e deixamos cozinhar uma hora mais.

Pronto!! Bom apetite 😋

