---
title: "Sistem de autoirigare"
description: "Sistem de autoirigarea plantelor creat cu ajutorul la Arduino - Wlog elaborat de Vladimir Rusnac"
pubDate: "24 april 2024"
heroImage: "/src/assets/coperte/arduino-watering.png"
badge: "Eco"
---


![Proiectul nostru](/img/arduino-earth.png)

## Problema abordată

Seceta din ultimul timp a afectat grav toate
culturile agricole din Republica Moldova. Rezolvarea acestei probleme,
iar anume utilizarea sistemelor de irigare a culturilor a creat o nouă
problemă care are capacitatea de a devei una globală, iar anume:
*utilizarea irațională a apei.*

![Am putea preveni seceta în Moldova? Soluții pe termen scurt și mediu
--- Moldova.org](/img/seceta.png)

## Soluția

Sistem inteligent de **auto-irigare** - folosind un kit de microelectronică
bazat pe [**Arduino**](https://www.arduino.cc/) și unelte ajutătoare, echipa noastră a elaborat un
dispozitiv care are capabilitatea de a analiza umeditatea solului și a
uda plantele în momentul potrivit.

![Sisteme de irigare -
alexgarden.md](/img/auto-irigare.png)

## Avantaje

Un astfel de proiect poate fi adaptat pentru irigarea grădinilor și
livezilor care necesită o mare cantitate de apă, acest sistem
intelligent va consuma apa rational, evitând irosirea apei și
suprahidratarea plantelor

![How kids can save water \| Edge Early
Learning](/img/picatura.png)

Potențialul de a moderniza sistemul. Microcomputerul Arduino poate
folosi diferite senzoare care pot ajuta pentru monitorizarea solului
pentru a mări recolta.

## Dezavantaje

Sistemul intelligent de auto irigare este scump în producție, un set
arduino costă `24$`, ce nu este rentabil pentru grădini mici.

## Principiu de lucru

Arduino este un minicomputer cu capacitatea de a prelucra datele primite
de senzori și să transmită instrucția către partea executivă.

![irigatorul](/img/prototipul.png)

<img alt="arduino-uno" src="/img/arduino-uno.png" class="w-2/3"></img>

![soil-sensor](/img/soil-sensor.png)

![pump](/img/pump.png)

## Proiect elaborat de

**Vladimir Rusnac**

**Yehoward Rudenco**
