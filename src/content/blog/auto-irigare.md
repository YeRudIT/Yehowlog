---
title: "Sistem de autoirigare"
description: "Un sistem cu ajutorul lui Arduino pentru irigarea plantelor"
pubDate: "3 february 2024"
heroImage: "/src/assets/coperte/arduino-watering.png"
badge: "Eco"
---

## Problema

![farmin](/img/farmir.jpg) 

Fu un an secetos, nici ploaie, nici rouă. Bunelul își pregăti o cisternă de apă 
din care aproape în fiecare zi iriga plantele. Ce nu era bine, solul plantelor devenea lutos și tare.
Ne-am pus atunci pe gânduri, nu e posibil de automatizat procesul ducându-l la perfecțiune?
Nu este posibil de făcut un sistem de irigare, care v-a uda florile numai când ele într-adevăr necesită apă?

Luând în considerare că suntem o țară agricolă,
un astfel de sistem le-ar fi benefic și agricultorilor țării, mai ales în perioade de secetă.

## Soluția

![Proiectul nostru](/img/arduino-earth.png) 

Fiind inspirați de ideea simbiozei dintre plante și tehnologii, am decis să facem 
un sistem de auto-irigare. Un sistem simplu creat cu ajutorul:
+ **[Arduino](https://www.arduino.cc/)** - o platformă de procesare open-source, bazată pe software și hardware ușor de utilizat. Este construită în jurul unui procesor de semnal și este capabilă de a prelua date din mediul înconjurător printr-o serie de senzori și de a efectua acțiuni asupra mediului prin intermediul diferitelor dispozitive mecanice, precum luminile, motoarele, servomotoarele etc.

![arduino-uno](/img/arduino-uno.png) 

+ Senzor pentru detectarea umidității solului.

![soil-sensor](/img/soil-sensor.png) 

+ Pompiță electrică pentru pomparea apei.

![pump](/img/pump.png) 


Detectând nivelul de umiditate scăzut a solului, Arduino v-a porni pompa astfel irigând planta.

## Avantaje


### Reducerea consumului de apă

Un astfel de proiect poate fi adaptat pentru irigarea grădinilor și livezilor,
unde pot fi utilizate cantități enorme de apă. Un astfel de sistem inteligent v-a
consuma apa rațional, evitând supra-hidratări de apă.

![salveaza apa](/img/water-saving.jpg) 

### ECOnomisirea timpului

![time](/img/time.png) 

Deseori noi fiind preocupați uităm să irigăm scumpele noastre generatoare de oxigen.
Sistemul dat v-a lua asupra lui această responsabilitate, evitând uscarea unor plante nevinovate
și eliberându-ne timp pentru chestii mai importante. 

![body buolding](/img/freetime.png)

## Dezavantaje

### Tehnologii scumpe

![no money?](/img/no-money.jpg) 

Un Arduino-Uno (folosit în proiectul nostru) costă
**[24$](https://store.arduino.cc/collections/boards-modules/products/arduino-uno-rev3)**,
ne luând în considerare senzori, pompa, etc. Acesta nu este un preț
colosal de mare, dar totuși mulți vor prefera să îngrijească singuri plantele.



## Prototip

![irigatorul](/img/prototipul.png) 

Din păcate nu am reușit să creăm un produs complet viabil, fiind limitați de timp.
Cu toate acestea prototipul nostru demonstrează simplitatea unui astfel sistem de irigare automat.

----

Proiect elaborat de elevii **Liceului Teoretic "Grigore Vieru"** or. Briceni ![Vlad](/img/Vlad.png)  **Vladimir Rusnac**
în colaborare cu 
![Yehoward](/img/yehostark.webp)
**Yehoward Rudenco**.

