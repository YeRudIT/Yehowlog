---
title: "Venus &female;"
description: "Planeta Venera"
pubDate: "24 march 2024"
heroImage: "/src/assets/coperte/venera-header.png"
badge: "beta"
includeMatex: true
---


**Venus** cunoscut și ca **Luceafărul** este o planetă ***terestră(telurică)***, a II-a de la soare.
![sistem solar](/img/venera/venera-system.png)
Este al treilea cel mai luminos obiect natural de pe cer, avînd [magnitudinea aparentă](https://wiki.froth.zone/wiki/Magnitudine_aparent%C4%83?lang=ro) de **-4.14**.

|Date||
|-|-|
|Raza medie|6052km|
|masa|<ma-tex>4,8675\cdot1024 kg</ma-tex>|
|temperatura medie|<ma-tex>464\degree C</ma-tex>|
|rotație în jurul axei sale| 242 zile terestre|
|revoluție în jurul soarelui| 225 zile terestre|
|distanța maximă de la soare|	0.728213 AU (108.94 milioane km)|
|distanța minimă de la soare|	0.718440 AU (107.48 milioane km)|
|||

## Dimensiuni

După dimensiuni Venus are aproximativ aceiași masă și mărime ca pămîntul,
masa planetei fiind 81.5% din masa pămîntului,
și diametrul fiind numai cu 638km mai mic decît cel a pămîntului. 
Din cauza aceasta uneori Venus este numită **sora pămîntului**.


## Atmosfera
<img class="w-1/2 mb-0" src="https://wiki.froth.zone/media/wikipedia/commons/thumb/5/54/Venus_-_December_23_2016.png/600px-Venus_-_December_23_2016.png" alt="venus în ultraviolet"></img>

<div class="text-center mt-0 leading-3 italic font-medium">Norii venerei văzuți cu ajutorul razelor ulta-violete</div>

Venus are o atmosferă densă, compusă 96.5% din bioxid de carbon, 3.5% azot.
Ambele elemente fiind în stare supercritică[^supercritic] la suprafața
planetei, avînd o densitate de 65 <ma-tex>\frac{kg}{m^3}</ma-tex>. La fel sunt
și urme de alte gaze. Masa atmosferei este de 92 ori mai grea ca cea a pămîntului,
iar presiunea de 93 de ori mai mare ca cea a pămîntului, aceasta este
echivalentă cu presiunea la adîncimea de 1 km în oceanul terestru. Atmosfera
Venerei este de 50 de ori mai densă decît cea a pămîntului (la nivelul mării la
temperaturi de <ma-tex>20\degree C</ma-tex>).

Concentrația de bioxid de carbon în atmosferă generează cel mai puternic efect
de seră din sistemul solar. Aceasta face Venera cea mai fierbinte planetă din
sistemul solar, fiind mai fierbinte chiar decît **Mercur**.


## Nume

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/1863_Alexandre_Cabanel_-_The_Birth_of_Venus.jpg/800px-1863_Alexandre_Cabanel_-_The_Birth_of_Venus.jpg" alt="venera" class="w-1/2"></img>

În multe culturi planeta Venus era numită în cinstea zeiței dragostei și
frumuseței. La romani
[**Venus**](https://ro.wikipedia.org/wiki/Venus_(mitologie)?lang=ro), la greci
[**Afrodita**](https://en.wikipedia.org/wiki/Aphrodite?lang=en), la sumerieni
[Inanna/Iștar](https://en.wikipedia.org/wiki/Inanna?lang=en).

Planeta mai purta numele de **Lucifer**(aducător de lumină, steaua dimineții, de unde derivă **Luceafărul**),
sau **Vesperus**(aducător de seară, steaua serei). 

<img alt="lucifer" src="https://wiki.froth.zone/media/wikipedia/commons/thumb/f/f7/Alexandre_Cabanel_-_Fallen_Angel.jpg/250px-Alexandre_Cabanel_-_Fallen_Angel.jpg"></img>


## Faze

![faze](https://upload.wikimedia.org/wikipedia/commons/1/11/Phases_Venus.jpg) 

La fel ca și luna, Venus are faze.

## Orbite

![orbita](https://upload.wikimedia.org/wikipedia/commons/b/b7/Venusorbitsolarsystem.gif) 

Venus orbitează soarele la o distanță aproximativ 0.7ua (108 milioane km), completînd o revoluție în 224,7 zile (terestre).
Deși orbitele tuturor planetelor sunt eliptice, orbita lui Venus este foarte apropiată de un cerc.

Cînd Venera se află între pămînt și soare, fenomen numit [conjuncție
inferioară](https://wiki.froth.zone/wiki/Conjunc%C8%9Bie_(astronomie)?lang=ro),
ea se apropie de pămînt mai mult ca orice altă planetă. Cu toate acestea, Venus
își petrece marea parte a timpului departe de pămînt, ceea ce face mercur cea
mai apropiată planetă de terra în majoritatea timpului.

## Rotație
<img alt="spin" src="https://imgs.search.brave.com/-XfJ1hYEShnsOW7eI-KuBWFCkHG6VtwOg6TcBt80KWE/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9tZWRp/YTIuZ2lwaHkuY29t/L21lZGlhL3YxLlky/bGtQVGM1TUdJM05q/RXhNR3QyYWpaeWFU/bHlPWEEyY1hKeWF6/TmtOMk5oYTJvNGNH/WjFZVEY2ZDJ0dWJH/WndhbXBuZWlabGNE/MTJNVjluYVdaelgz/TmxZWEpqYUNaamRE/MW4vZW5qNTBrYW84/Z01mdS9naXBoeS5n/aWY.gif" class="scale-x-[-1]">
</img>

Toate planetele din sistemul solar se rotesc contrar acelor ceasornice, cu
excepția lui Venus. El efectuează o revoluție în jurul axei sale odată la 243 de zile pămîntești.
Deoarece rotirea sa este atât de lentă, Venus este foarte aproape de o formă sferică

## Explorare

Prima navă spațială care a fost trimisă către Venus a fost sonda sovietică
Venera 1, lansată la 12 februarie 1961 ca parte a programului
[Venera](https://en.wikipedia.org/wiki/Venera?lang=en). Trebuia să ajungă la
Venus pe o traiectorie de coliziune, dar contactul cu ea s-a pierdut la 7 zile
de la decolare.


Prima misiune de succes către Venus (precum și prima misiune interplanetară de
succes din lume) a fost misiunea Mariner 2 a Statelor Unite, care a trecut la
14 decembrie 1962 la 34.833 km deasupra suprafeței lui Venus și a adunat date
despre atmosfera planetei.

![mariner 2](https://wiki.froth.zone/media/wikipedia/commons/thumb/9/90/Mariner_2.jpg/220px-Mariner_2.jpg) 
 
Sonda spațială sovietică Venera 3 a ajuns la suprafața lui Venus la 1 martie
1966. A fost primul obiect creat de om care a intrat în atmosfera unei alte
planete și a ajuns la suprafața acesteia. Sistemul de comunicații a eșuat însă
și sonda nu a returnat nici o dată spre Pământ.

O altă sondă a fost Venera 4, care a intrat în atmosferă la 18 octombrie 1967
și a făcut o serie de măsurători. Sonda a arătat că temperatura suprafeței era
mai fierbinte decât calculase Mariner 2, a determinat atmosfera Venusului,
aceasta fiind cu mult mai densă decît creatorii sondei anticipase.

![venera 9](https://wiki.froth.zone/media/wikipedia/commons/thumb/f/ff/Foto_de_Venera_9.png/800px-Foto_de_Venera_9.png) 
Venera 9 a returnat prima imagine de pe suprafața unei alte planete în 1975.

## Misiuni viitoare

![viitor](https://imgs.search.brave.com/tbh7rUPZZK20dx59n6JIc78_FEPpePKH4k6fvNzWZwc/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9hLnBp/bmF0YWZhcm0uY29t/LzI1MjR4MTQxOS9m/ODk4OWJiNWZiL3dv/b2R5LWFuZC1idXp6/LWxpZ2h0eWVhci1l/dmVyeXdoZXJlLXdp/ZGVzY3JlZW4uanBn) 

În iunie 2021, NASA a anunțat două potențiale misiuni pe Venus ca parte a
programului Discovery. Sonda DAVINCI+ urmează să fie o sondă atmosferică care
va analiza în detaliu compoziția atmosferei planetei și va fotografia
suprafața. Orbitatorul VERITAS va observa planeta și va realiza hărți radar noi
și detaliate, concentrându-se în special pe zonele de activitate potențială. Se
preconizează că sondele vor călători spre Venus în 2028–2030

n aceeași lună, ESA a anunțat că va trimite un orbitator EnVision pe Venus la
începutul anilor 2030 cu o misiune cuprinzătoare de a studia atmosfera, a
observa suprafața și a studia geologia

---

[^supercritic]:
    > Starea supercritică - starea a unei substanțe în care dispare diferența dintre starea lichidă și cea solidă
    >
    > -- [Wikipedia](https://wiki.froth.zone/wiki/%D0%A1%D0%B2%D0%B5%D1%80%D1%85%D0%BA%D1%80%D0%B8%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D0%B6%D0%B8%D0%B4%D0%BA%D0%BE%D1%81%D1%82%D1%8C?lang=ru)
