---
title: "Hidrații de carbon în alimentație"
description: "Descrierea glucidelor ce jocă rol principal în alimentație"
pubDate: "29 february 2024"
heroImage: "/src/assets/coperte/baking-bread.png"
includeMatex: true
---

## Ce sunt Hidrații de carbon

Hidrații de carbon (zaharidele sau glucide) reprezintă una dintre cele 
mai importante și răspândite clase de compuși organici naturali, 
alcătuind cca 80% din masa uscată a componentelor regnului vegetal. Glucoza, fructoza, zaharoza, amidonul, celuloza 
reprezintă clasa hidraților de carbon.
Alături de grăsimi și proteine, hidrații de carbon participă 
la schimbul de substanțe din organismul omului și al animalelor,
furnizează energia necesară activității vitale.

Denumirea hidrați de carbon este legată de formula generală, în care se înscriu reprezentanții 
acestei clase de compuși: <ma-tex>\ce{ C$n$H2$n$O$n$ }</ma-tex> sau <ma-tex>\ce{C$n$(H2O)$n$}</ma-tex>.
Numele zaharide sau glucide se datorează gustului 
dulce al unora dintre ele (lat. saccharum – "zahăr", gr. glycos – "dulce").


## Glucoza

<!--TODO: adauga formula -->

![glukoza](/img/hidrati/glukoza.png) 

![glucoza](/img/hidrati/glucoza.png) 


<ma-tex class="text-2xl inline-block w-full text-center">C\_6H\_{12}O\_6</ma-tex>


Glucoza este o substanță cristalină, incoloră, 
dulce la gust, bine solubilă în apă.

![glucoză pudră](/img/hidrati/al-capucino.png) 

Glucoza se obține în natură în urma fotosintezei.
În natură, glucoza este răspândită în toate organele regnului vegetal: rădăcini, tulpini, frunze, fructe.

<ma-tex> \ce{6CO2 + 6H2O ->[h\nu] C6H12O6 + 6O2 ^}</ma-tex>



Organismele vii nu produc hidrați de carbon, ci îi obțin cu hrana.
Glucoza este una din principalele surse energetice ale organismelor vii.
Ea ajunge în organism împreună cu hrana, fie în stare liberă, fie sub formă legată, ca parte componentă a zaharozei sau a amidonului (conținut în cartofi, pâine, orez etc.).

![fruits](https://imgs.search.brave.com/sjAhraNoLwTc1t-UVASTtgwlNyRhE1EoEnN3S1HQYOQ/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pLmd1/aW0uY28udWsvaW1n/L21lZGlhLzIwYTgw/N2EzMWFjYjZkZTNi/YzgyNjhhMmFlMWNi/ZDhmMjNiMmQ0YjAv/MTc1XzI3MV80MDEy/XzI0MDcvbWFzdGVy/LzQwMTIuanBnP3dp/ZHRoPTQ2NSZkcHI9/MSZzPW5vbmU) 
<div class="flex flex-row mt-[-50px] gap-6 items-center">

![svecla](https://imgs.search.brave.com/HXMKSUC6XJ4PgLBDS8TG07n883u2TFlzn19BQM1sCd8/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9kb2Z0/b3JpYS5yby93cC1j/b250ZW50L3VwbG9h/ZHMvMjAxOS8wMi9z/ZmVjbGEtZGUtemFo/YXItNzAweDQ0MC5q/cGc) 


![tulpina](https://imgs.search.brave.com/N0O5yPwkBLTjLgp_Xq7QXi_zB18xPy3Dy5nm3mdmtoU/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9taW5l/Y3JhZnRob3d0by5j/b20vaW1hZ2VzL3Bh/Z2UtbWluZWNyYWZ0/LXN1Z2FyLWNhbmUt/ZmFybS5wbmc) 

</div> 

În organism, amidonul hidrolizează până la glucoză sub acțiunea 
enzimelor gastrice și intestinale. Glucoza, bine solubilă în apă, trece 
prin pereții intestinali și ajunge în sânge, de unde este transportată 
spre diverse organe. În celule, o parte din glucoză „arde“ (se oxidează),
furnizând energia necesară activității vitale. Procesul complicat 
de oxidare poate fi redat prin următoarea ecuație sumară:
<!--TODO: ecuația de oxidare -->

<ma-tex>\ce{C6H12O6 + 6O2 -> 6CO2 + 6H2O + Q}</ma-tex>



Toate organele interne utilizează, ca surse de energie, 
hidrați de carbon, proteine, 
grăsimi. Excepție face creierul 
uman, pentru care sursa de 
energie este glucoza. Astfel, 
alimentarea noastră zilnică cu 
produse ce furnizează glucoză 
este o necesitate vitală.


Glucoza este o componentă indispensabilă sângelui, dar conținutul ei trebuie să se mențină în limitele de 0,07–0,11%. 
Dacă acesta depășește limita admisibilă, atunci se dereglează schimbul de zaharide și se 
dezvoltă diabetul zaharat, boală care afectează organismul uman.

![glucoza in singe](https://imgs.search.brave.com/Ev-LgZVtJ7-7ThQBTOVdCUHqiEvL5ZSrHLxejeQI1j8/rs:fit:860:0:0/g:ce/aHR0cHM6Ly90NC5m/dGNkbi5uZXQvanBn/LzAyLzc2LzY0Lzcz/LzM2MF9GXzI3NjY0/NzMxMl9mbnhpQVg3/TXNiQnJsUW1rNjJW/UkpxOTFRRFhqSU1h/by5qcGc) 

Importante sunt transformările fermentative ale glucozei. Există mai multe tipuri de fermentări (alcoolică, lactică, butirică, citrică 
ș.a.), care decurg sub acțiunea enzimelor specifice.
Fermentare alcoolică:

<ma-tex> \ce{C6H12O6 ->[enzime] 2C2H5OH + 2CO2 ^} </ma-tex>
<!-- Din Glucoză se obține alcool hehe. -->
<!-- samagonshiki -->

![samogon](https://cdn.homedistil.ru/fs/1603/19/20460.269377.jpeg) 


## Fructoza
![fructose](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Table_fructose.JPG/800px-Table_fructose.JPG) 

![glucoza](/img/hidrati/fructoza.png) 

<ma-tex class="text-2xl inline-block w-full text-center">C\_6H\_{12}O\_6</ma-tex>


Fructoza, de rând cu alți hidrați de carbon, se obține prin fotosinteză.
Ea se găsește în fructe, struguri, miere. Este o substanță 
cristalină, bine solubilă în apă, de 1,5 ori mai dulce decât zahărul și 
de trei ori mai dulce decât glucoza.

Fructoza, spre deosebire de glucoză, se asimilează mai ușor, nu 
necesită insulină și de aceea este recomandată bolnavilor 
de diabet zaharat.

Glucoza și fructoza, în cantități egale, sunt componenta principală a mierii de albine.

![bee milk](http://beekeeping.ge/images/Honey/Runny_hunny.jpg) 


## Zaharoza

![zahar](http://pluspng.com/img-png/sugar-cube-png-transparent-image-2850.png) 

![zaharoza schema](/img/hidrati/zaharoz.png) 

<ma-tex class="text-2xl inline-block w-full text-center">C\_{12}H\_{22}O\_{11}</ma-tex>

Zaharoza (zahărul alimentar) este o substanță cristalină, bine solubilă în apă, dulce la 
gust. Se topește la 160<sup>o</sup>C. Fiind încălzită până la 180<sup>o</sup>C și răcită brusc, 
se transformă într-o substanță amorfă cu aspect și gust de caramelă.

![bumboane](http://4.bp.blogspot.com/-mQgPcaBztR8/ULvsiyd8pdI/AAAAAAAAIK0/q2rQoGuHj9s/s1600/IMG_0510.JPG) 

În natură, zaharoza se găsește în morcovi, 
în frunzele și semințele multor plante, în fructe (caise, piersici, 
pere), în sucurile de mesteacăn, de palmier, de arțar.
Cantități considerabile de zaharoză (15–20%) se conțin în sucul de sfeclă și în cel 
de trestie-de-zahăr, din care este extrasă această substanță.

<div class="flex flex-row mt-[-50px] gap-6 items-center">

![trestie](http://media.releasewire.com/photos/show/?id=240004) 

![svecla](https://foodsguy.com/wp-content/uploads/2020/12/Best-Beet-Sugar-Brands.jpg) 

</div>

În Republica Moldova, zaharoza este extrasă din sfecla-de-zahăr, 
plantată anual pe suprafețe mari în partea de nord și în cea centrală.

<!-- TODO: spune din cuvintele tale -->
<!-- TODO: imaginile procesului de obținere-->
<!-- Tehnologia de prelucrare a sfeclei-de-zahăr începe cu spălarea și mărunțirea. Urmează  -->
<!-- extragerea cu apă caldă (80oC), prin contracurent, a zaharozei și eliberarea acesteia de alte  -->
<!-- componente. În acest scop, apele de extragere sunt tratate cu apă de var. Se formează un se- -->
<!-- diment de proteine și de săruri insolubile de calciu ale acizilor fosforic, oxalic și citric. -->

## Amidonul

![amidon](https://www.toutvert.fr/wp-content/uploads/2016/03/shutterstock_1220173258.jpg) 

![amidon schelet](/img/hidrati/amidon.png) 

<ma-tex class="text-2xl inline-block w-full text-center">(C\_6H\_{10}O\_5)\_n</ma-tex>

Amidonul este un polimer natural care se formează în frunzele 
verzi în urma procesului de fotosinteză. Are o mare răspândire în 
natură și este principalul hidrat de carbon de rezervă al plantelor. 
Cantități considerabile de amidon conțin orezul, porumbul, cartoful

<div class="flex flex-row mt-[-50px] gap-6 items-center">

![orez](https://www.navitalo.com/wp-content/uploads/2021/01/navitalo-flour-starches-rice-starch-icon.jpg) 
![papșoi](https://cdn.futura-sciences.com/buildsv6/images/wide1920/6/6/2/6628d2b41d_132381_amidon-c-panya168-fotoliacom-01.jpg) 

![cartof](https://starchinfood.eu/wp-content/uploads/sites/10/2022/06/iStock-1310169023.jpg) 

</div>

Amidonul reprezintă un praf alb, 
puțin solubil în apă, asemănător cu făina de grâu. Se umflă în apă 
fierbinte, dând un amestec coloidal, numit clei de amidon (pap de 
amidon).

Rolul biologic. Amidonul este una dintre sursele 
energetice importante ale organismelor uman și animal.
Nimerind în organism cu hrana, amidonul se 
supune hidrolizei enzimatice, transformându-se în 
glucoză. Aceasta este transportată spre celule, unde 
se consumă parțial pentru necesitățile energetice ale 
organismului, conform schemei:
<!--TODO: adauga formula -->

<ma-tex>\ce{C6H12O6 + 6O2 -> 6CO2 + 6H2O + Q}</ma-tex>

Glucoza care nu a fost consumată se combină 
din nou, formând un compus macromolecular – glicogenul – cu aceeași formulă moleculară
<ma-tex>(C\_6H\_{10}O\_5)\_n</ma-tex>,
dar mult mai ramificat decât amidonul.
Glicogenul este rezerva energetică a organismului între mese și în cazul unor eforturi sporite.
El se consumă după același principiu ca și amidonul.
Glicogenul se depune în ficat și în mușchi. Dacă 
conținutul de glicogen depășește limita de 50–60 g 
la 1 kg de masă, organismul încetează să-l sintetizeze,
restul glucozei transformându-se în grăsime.

Amidonul este una dintre principalele surse alimentare. Produșii hidrolizei 
parțiale și totale a amidonului (dextrinele și glucoza) se asimilează mai ușor,
de aceea pregătirea bucatelor din cartofi, din porumb sau cereale presupune o prelucrare termică (fierbere, 
prăjire, coacere), însoțită de hidroliza amidonului. Apariția unei pojghițe rumene pe pâinea 
coaptă sau pe cartofii prăjiți se datorează formării dextrinelor cleioase.

![pîine](http://upload.wikimedia.org/wikipedia/commons/7/71/Anadama_bread_(1).jpg) 

Metoda de obținere. Amidonul este extras din cartofi și din porumb. Materia primă 
este mărunțită și depusă pe site, unde este spălată din abundență cu apă. Granulele de amidon
sunt antrenate de șuvoiul de apă, trec prin sită și se depun pe fundul vasului. După 
spălare și decantare, amidonul este uscat la presiune redusă.


<iframe id='ivplayer' src='https://inv.nadeko.net/embed/PgKR6EsKl5I?t=212' class="m-auto w-full h-96"></iframe>

## Celuloza

![vata](https://tissueonline.com.br/novo/wp-content/uploads/2020/11/15_texturafluff.jpg) 

![celuloza  skema](/img/hidrati/celuloza.png)

<ma-tex class="text-2xl inline-block w-full text-center">(C\_6H\_{10}O\_5)\_n</ma-tex>

Celuloza este materialul de construcție al pereților celulelor vegetale, de unde provine și denumirea. 
Celuloza conferă plantelor stabilitate și elasticitate.

![celula celuloza](http://desertbruchid.net/4_GB_Lecture_figs_f/4_GB_03c_OrgChem_Fig/3_8_mad7_Cellulose_Polys.GIF) 

La fel ca amidonul, celuloza este un produs al procesului natural 
de fotosinteză. Conținutul celulozei în reprezentanții regnului vegetal 
variază: fibrele de bumbac, de in, de cânepă sunt alcătuite, practic,
numai din celuloză (până la 98%), lemnul conține circa 50% de celuloză.

<div class="flex flex-row mt-[-50px] gap-6 items-center">

![in](https://www.thespruce.com/thmb/TV8PqWqv_R9UEiFgyotAvMwhImk=/2119x1414/filters:no_upscale():max_bytes(150000):strip_icc()/flaxGettyImages-577737424-bbf545865533439e933eafd5061188a6.jpg) 

![bumbac](https://www.brittslist.com.au/wp-content/uploads/2019/07/Australian-cotton-bolls.jpg) 
![cinepa](https://media.greenmatters.com/brand-img/NAmlaRndm/1440x754/advantages-disadvantages-of-hemp-fabric1-1605200378340.jpg) 

</div>

Celuloza este o substanță solidă, fibrilară, insolubilă în apă și în dizolvanți organici, cu o stabilitate 
mecanică înaltă.

Ca și amidonul, celuloza este un polimer natural cu formula moleculară <ma-tex>(C\_6H\_{10}O\_5)\_n</ma-tex>.

Celuloza din bumbac, din in sau cânepă este, practic, pură și poate fi folosită după o tratare neesențială. 
Dar aceste culturi tehnice necesită anumite condiții climaterice și o 
îngrijire specială. Chimiștii-tehnologi au elaborat procedee de extragere a celulozei din lemn.

![foi](https://blog.resolutefp.com/wp-content/uploads/2016/01/paper.jpg) 

La hidroliza totală a celulozei, se formează glucoză din care, prin fermentare, se obține 
alcool etilic tehnic.

![samogon](https://m.media-amazon.com/images/M/MV5BNzFlNDllMTgtOWU2NC00YWYzLTliNWItN2M3OTM4ODAyODIyXkEyXkFqcGdeQXVyNjAwNTYwNDg@._V1_.jpg) 

## Lactoza

![cat](https://imgs.search.brave.com/M_oBaS0mO_9LguOsnX7L8WJ_tPJM2QGlHHX8yC3jXug/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9pLnBp/bmltZy5jb20vb3Jp/Z2luYWxzLzRhLzY1/LzFjLzRhNjUxYzA2/NWFlMmRmOTQyYjk2/Yjc2OTc1YjAyODY3/LmpwZw) 

![lactoza skem](/img/hidrati/lactoza.png)

<ma-tex class="text-2xl inline-block w-full text-center">C\_{12}H\_{22}O\_{11 }</ma-tex>

Lactoza este o glucidă care poate fi întâlnită în lapte. Numele de lactoză provine din latină: lac,
lactis - lapte iar terminația oză se adaugă glucidelor.

Lactoza este o substanță cristalizată incoloră și inodoră cu gust dulceag care conține între 25 și 60 % zaharoză.

Lactoza, ca parte componentă a laptelui, este importantă în alimentarea mamiferelor tinere. Ea joacă un rol în stimularea digestiei prin scindarea ei de către enzima lactază, în glucoză și galactoză. Adulții produc această enzimă într-o cantitate mai redusă ca tineretul. La unii indivizi poate apărea intoleranța față de lactoză, acest lucru fiind explicat prin lipsa lactazei. Printre funcțiile lactozei se poate aminti că ea oferă organismului energie, stimulează absorbția calciului, frânează dezvoltarea bacteriilor care produc putrefacție, stimulează dezvoltarea bacteriilor bifide și are un rol laxativ.

## Concluzie

![suntem diferiti mem](/img/hidrati/suntem-diferiti.png) 

Alimentația nu poate exista fără glucide. Ele fiind sursa principală de energie și de dulceață în viața noastră.
