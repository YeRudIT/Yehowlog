---
title: "İAZAR: Model pentru transcrierea vocii"
description: "Antrenăm un model de transcriirea vocii în text cu date în română"
pubDate: "08 june 2024"
heroImage: "/src/assets/coperte/iazar.png"
badge: "AI"
---
<!-- scrim autorii -->

## Iazar

<img src="/img/iazar/iazar-2.png" class="w-52">

[Proiectul](https://github.com/yehoward/iazar) dat este elaborat în cadrul competiției `Tekwill Junior Ambasadors`.

### De ce Iazar?

**yazar** în limba turcă semnifică *`scriitor`*,
drept `IA + yazar = IAzar`, inteligență artificială scriitoare.

## Scopul proiectului

Deseori aplicațiile care utilizează transcrierea vocii nu înțeleg anumite graiuri.
Aceasta poate fi deranjant mai ales la transcrierea mesajelor vocale, unde vorbitorii
tind să vorbească în graiul matern.

Scopul nostru este să antrenăm un model de transcrierea vocii în text, să
transcrie [graiul matern Moldovenesc](https://wiki.froth.zone/wiki/Graiul_moldovenesc?lang=ro).

<img src="https://i.pinimg.com/originals/1d/2a/48/1d2a48542df1b6df585c869798308002.png" alt="training coqui" class="w-1/2"></img> 


## Alte utilizări

Un astfel de model are mai multe utilități.
<!-- bun în jurnalistică, traducere, -->
<!-- transcrierea mesajelor vocale, -->
<!-- interacționarea persoanelor cu -->
<!-- dizabilități cu calculatorul, etc. -->

#### Mesajele Vocale

Textul transcris din mesajele vocale poate fi copiat, tradus. Textul fiind în genere mai ușor de utilizat, 
și mai comod de-l citit. <strong>Ăsta v-a fi scopul în jurul căruia vom lucra</strong>.

#### Subtitrări:

Textul transcris dintr-un video poate fi folosit pentru subtitrări.

#### Jurnalistică:

Jurnaliștii pot să folosească textul transcris din întrevederile lor în articole,
care eventual poate fi traduse și în alte limbi. De asemenea textul transcris poate fi folosit pentru subtitrarea întrevederilor.

#### Persoanele cu dizabilități

Modelul poate fi extins pentru a ajuta persoanele cu dizabilități.


## Datele

<!-- https://commonvoice.mozilla.org/en/datasets -->

Date pentru antrenarea în limba română a unui model de transcrierea 
vocii sunt oferite de către [Mozilla](https://commonvoice.mozilla.org/ro/datasets)

![mozila](https://commonvoice.mozilla.org/dist/cv-logo-black.270d5891c1700962.svg) 

<!-- povistim despre formatul datelor (csv, tsv, și ce nu) și ce nu -->

### Explorarea


În set de date găsim:

<pre class="font-mono leading-none text-xl">
<code >
<i class="nf nf-md-folder"></i> cv-corpus-15.0-2023-09-08
├── <i class="nf nf-md-folder_music"></i> clips
│  ├── <i class="nf nf-md-music"></i> common_voice_ro_38436395.mp3
│  ...
├── <i class="nf nf-md-file_table"></i> clip_durations.tsv
├── <i class="nf nf-md-file_table"></i> dev.tsv
├── <i class="nf nf-md-file_table"></i> invalidated.tsv
├── <i class="nf nf-md-file_table"></i> other.tsv
├── <i class="nf nf-md-file_table"></i> reported.tsv
├── <i class="nf nf-md-file_table"></i> test.tsv
├── <i class="nf nf-fa-file_text"></i> times.txt
├── <i class="nf nf-md-file_table"></i> train.tsv
└── <i class="nf nf-md-file_table"></i> validated.tsv
</code>
</pre>


Toate fișierele (cu excepția:
`reported.tsv`,
`clip_durations.tsv`)
conțin cîte o coloană cu identificatoare, calea spre audio, 
transcripția, numărul de aprecieri de la validare, vîrsta, genul, și altele mai puțin importante.

Fișierul `validated.tsv` conține descrieri despre toate audiourile validate de voluntarii de la **common voice**, fiind apreciate relativ înalt.
Fișierul `invalidated.tsv` conține descrieri despre audiouri cu aprecieri negative din partea voluntarilor.


Fișierul `train.tsv` este folosit pentru antrenarea modelului,
fișierul `dev.tsv` este folosit pentru validarea modelului, și
fișierul `test.tsv` este folosit pentru testarea modelului.

Fișierul `other.tsv` conține descrieri despre audiouri de rezervă.

Fișierele `times.txt` și `clip_durations.tsv` descriu durata audiourilor.

În fișierul `reported.tsv` sunt lista de date care au anumite erori, aceste fu raportate de către voluntari.
Erorile puteau fi utilizarea alfabetelor străine, greșeli de lexic sau pronunție, date pentru limbă străină, etc.

În mapa `./clips/` se află audiourile.

<!-- Datele trebuie să se supună alfabetului român. Erau cazuri cînd nu o făcea. -->
<!-- Alfabetul tot era dracos -->

### Prelucrarea

Pentru simplitate noi am eliminat toate datele cu erori descrise în `reported.tsv`.

<!-- Sed https://www.gnu.org/software/sed/ -->
<!-- grep https://www.gnu.org/software/grep/ -->

Am utilizat utilitățile GNU pentru a șterge toate datele defectate.

```fish
for i in $(awk -F '\t' '{print $1}' reported.tsv | sed '1d' )
    for audio_name in $(grep $i *tsv | awk -F'\t' '{print $2}')
        if test -f "./clips/$audio_name"
            rm "./clips/$audio_name"
        end
        sed -i "/$audio_name/d" *.tsv
    end
end
```




- Prima buclă `for` parcurge fiecare valoare din prima coloană a fișierului `reported.tsv` utilizînd comanda [`awk`](https://www.gnu.org/software/gawk/) (se utilizează comanda [`sed`](https://www.gnu.org/software/sed/) pentru a exclude prima linie).
Aceasta fiind textele datelor cu erori.

- În interiorul primei bucle `for`, se deschide o altă buclă `for` care parcurge fiecare linie `$i`, ce conține textul defectat, găsită cu ajutorul comenzii [`grep`](https://www.gnu.org/software/grep/) din toate fișierele cu extensia `tsv` și cu ajutorul comenzii [`awk`](https://www.gnu.org/software/gawk/) se extrage coloana a 2-a, adică denumirea fișierului audio.

- În interiorul celei de a doua bucle, se verifică dacă există fișierul în mapa `./clips/` cu numele specificat de variabila $audio\_name (denumirea fișierului audio). Dacă există, fișierul respectiv este șters folosind comanda `rm`.

- După ștergerea fișierului, se utilizează comanda [`sed`](https://www.gnu.org/software/sed/) pentru a elimina toate liniile care conțin valoarea $audio\_name (adică numele fișierului audio defectat) din toate fișierele cu extensia `tsv`.


<!-- alte utilități de bază "neuxni"(chipurile gnu nu-i unix). -->

## Coqui

![coqui](https://stt.readthedocs.io/en/latest/_static/coqui-STT-circle.png) 

Pentru a-mi atinge scopurile ințial am ales modelul [Coqui Speech To Text](https://stt.readthedocs.io/en/latest/DEPLOYMENT.html).

<!-- De ce modelul asta? (în marea parte că are broscuță ca avatar) -->
<!-- Bine documentat, permite modificarea simplă a datelor de la common-voice, etc. -->
<!-- TODO: Explică mai detaliat IDK -->
#### Avantaje 

- Este un model foarte bine [documentat](https://stt.readthedocs.io/en/latest/DEPLOYMENT.html), dotat cu explicații bune și exemple.

- Are drept logo o broscuță drăguță   <img width="40px" class="m-0 ml-2 inline" src="https://stt.readthedocs.io/en/latest/_static/coqui-STT-circle.png" alt="coqui"></img>

- Poate [transforma](https://stt.readthedocs.io/en/latest/COMMON_VOICE_DATA.html#common-voice-data) datele de la **common voice** în formatul pe care-l necesită.
<!-- TODO: Comanda pentru transformare --> 

<pre class="font-mono leading-none text-l">
<code>
$ bin/import_cv2.py --filter_alphabet calea/căstre/un/alphabet.txt /calea/către/archivul/common-voice/extras
</code>
</pre>

Aceasta v-a crea cîte un `wav` fișier din fiecare fișier `mp3`, și cîte un `csv` fișier.
Fișierele `csv` vor conține:
<pre>
 `wav_filename` - calea către fișierul audio.
 `wav_filesize` - mărimea fișierilor în biți, folosită pentru sortare.
 `transcript` - transcripția la audio.
</pre>



#### Dezavantaje

- Din păcate proiectul nu mai este menținut de dezvoltatori 
- Din cauza că proiectul a fost abandonat de dezvoltatori, el necesită dependențe învechite. 
Din cauza aceasta pot apărea probleme cu setarea modelului


<!-- cum l-am antrenat https://stt.readthedocs.io/en/latest/-->
<!-- copy pasta -->

## Antrenarea modelului

<!-- Donker and wasnot -->
<!-- Inspirăm de la coqui trainbook -->

<img class="w-1/3" alt="docker" src="https://logos-world.net/wp-content/uploads/2021/02/Docker-Symbol.png"></img> 

Pentru antrenarea modelului am folosit [`Docker`](https://www.docker.com/why-docker) 
după sfatul a <a class="no-underline" href="https://i.kym-cdn.com/photos/images/original/002/052/260/d94.png">dezvoltatorilor coqui</a>.
`Docker` este o aplicație folosită pentru a simplifica crearea aplicațiilor, implementarea lor și rularea lor folosind `containere`.
Care permite dezvoltatorului să împacheteze aplicația împreună cu toate componentele necesare.
Aceasta garantează ca aplicația să poată lucra pe orice sistem nu numai pe a dezvoltatorului.

<img class="w-1/2" alt="docker meme" src="/img/docker-is-born-meme.jpg"></img> 


După ce am setat [`docker`](https://stt.readthedocs.io/en/latest/playbook/ENVIRONMENT.html#install-docker), am instalat modelul cu ajutorul [comenzii](https://stt.readthedocs.io/en/latest/playbook/ENVIRONMENT.html#pulling-down-a-pre-built-coqui-stt-docker-image):


<pre class="font-mono leading-none text-l">
<code>
$ docker pull ghcr.io/coqui-ai/stt-train:latest
</code>
</pre>

Această comandă descarcă cîțiva Gb de date.
Descărcarea fiind destul de înceată.

Pentru accesarea `containerului` am utilizat comanda:

<pre class="font-mono leading-none text-l">
<code>
$ docker run -it  -v ./cv-corpus-15.0-2023-09-08/ro:/cv-data/ ghcr.io/coqui-ai/stt-train:latest
</code>
</pre>

`./cv-corpus-15.0-2023-09-08/ro/` fiind fișierul cu date de la **common voice**,
`/cv-data/` este denumire cu care vom putea accesa fișierul de la **common voice** din `container`.


Antrenăm inteligența cu următoarea [comandă](https://stt.readthedocs.io/en/latest/playbook/TRAINING.html):

<!-- TODO: scorer wasnot -->

<pre class="font-mono leading-none text-l">
<code>

$ python -m coqui_stt_training.train\
            --train_files /cv-data/clips/train.csv\
            --dev_files /cv-data/clips/dev.csv\
            --test_files /cv-data/clips/test.csv\
            --checkpoint_dir /cv-data/checkpoints_new\
            --dropout_rate 0.3\
            --scorer /cv-data/scorer/kenlm-romanian.scorer

</code>
</pre>


### Pași și epoci

>  În antrenament, **un pas** este o actualizare a gradientului; adică o încercare de a găsi cea mai mică sau minimă pierdere. Cantitatea de procesare efectuată într-un singur pas depinde de dimensiunea lotului.  În mod implicit, train.py are o dimensiune de lot de 1. Adică, procesează un fișier audio în fiecare pas.
>
> **O epocă** este un ciclu complet prin datele de antrenament. Adică, dacă aveți 1000 de fișiere listate în fișierul `train.tsv`, atunci vă veți aștepta să procesați 1000 de pași pe epocă (presupunând o dimensiune a lotului de 1)
>
> -- [Documentația coqui](https://stt.readthedocs.io/en/latest/playbook/TRAINING.html#steps-and-epochs) 

Putem deci simplu afla lungimea unei epoci, enumerînd cu ajutorul comenzii `wc` numărul de linii 
conținute de fișierul de antrenare `train.tsv` (excluzînd prima linie).

<pre class="font-mono leading-none text-xl">
<code>
$ wc train.tsv -l
5173 train.tsv 
</code>
</pre>

În cazul nostru, folosind diminsiunea loturilor implicită, o epocă v-a avea 5172 de pași.

### Experiența

<!-- TODO: situația -->

#### Pe calculator 8Gb RAM, Procesor AMD Rayzen 7 3700u

> Experiența ... dureroasă

În decurs de aproape o săptămînă, modelul n-a reușit să efectueze nici o epocă 

#### Pe calculator cu 15Gb RAM, procesor AMD Rayzen 5 5000

> Rapid de încet

Școala mi-a oferit un calculator pentru antrenarea Inteligenței Artificiale.
De acum modelul efectua o epocă în decurs de ~2,5 zile.
Este un progres considerabil comparativ cu mediul anterior. 
Dar oricum este puțin.
Conform documentației modelul are nevoie de ~75 de epoci<!--  TODO: date concrete -->
deci reiese ~187.5 zile pentru antrenarea completă a modelului...

<img src="https://i.imgflip.com/26bkez.jpg" alt="wait" class="w-1/2"></img> 

#### Pe calculator 8Gb RAM, procesor Intel i5 10400F, procesor grafic Nvidia 1650

<!-- TODO: Descrim ipoteza cu Nvidia -->

>*** Nu antrenați utilizînd numai procesorul. ***
> 
> Acest manual presupune că veți folosi GPU-uri NVIDIA. Antrenarea unui model de recunoaștere a vorbirii 🐸STT numai pe CPU va dura foarte, foarte, foarte mult timp. Nu antrenați cu procesorul.
>
>  -- [Documentația coqui](https://stt.readthedocs.io/en/latest/playbook/ENVIRONMENT.html) 


<div class="w-80 m-auto">

![confused kid](https://www.smarttechbuzz.org/wp-content/uploads/2020/11/confused-2.jpg)

</div> 

Acum suntem convinși faptul că pentru antrenarea modelului pe un calculator lipsit
de cartelă NVIDIA îmi va lua foarte mult timp.
<img src="https://media.vandal.net/i/620x526/8-2018/2018829111814_13.jpg" alt="nvidia meme" class="w-1/2">
</img>

Avînd din păcate numai 4 rinichi la dispoziție nu suntem în stare să cumpărăm
Nvidia. Din bucurie a fost găsit un voluntar care ne permise să utilizăm
computătorul său cu o placă grafică NVIDIA, astfel prevenind sacrificiul de
rinichi pentru buna cauză.

Am urmărit [documentația
Coqui](https://stt.readthedocs.io/en/latest/playbook/ENVIRONMENT.html) pentru
setarea mediului de antrenare. Din păcate, în pofida celor mai bune intenții
ale prietenului nostru, a orelor investite în setarea mediului, n-am reușit să
setăm mediul de antrenare pe
[Windows](https://yerudit.codeberg.page/blog/windows/) prin
[wsl(subsistema windows pentru linux)](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux).

#### Colab
<img src="https://i.imgflip.com/5u61ei.png" alt="me on colab" class="w-1/2">
</img>

Cu toate că coqui oferă [**caiete jupyter**](https://github.com/coqui-ai/STT/tree/main/notebooks),
lipsa de support din partea dezvoltatorilor ne-a ajuns însă și aici. 
Problema dependențelor în cazul dat fiind cea mai evidențiată.
Spre exemplu versiunea de `TenserFlow` 
necesitată de coqui nu mai era disponibilă în [`pypi`](pypi.org).

## Recunoaștere....

Curiozitatea și neștiința m-a făcut să ignor orice semne și orice sfaturi.
Chiar `coqui` recomanda alternative.
<a href="https://github.com/coqui-ai/STT"><img src="/img/coqui-sfat.png">
</img></a>

A venit timpul să nu le mai ignorăm, și să încercăm norocul pentru a-mi atinge scopurile...

## Whisper

<img src="/img/whisper.png" alt="openAI" class="">
</img>

[**Whisper**](https://openai.com/research/whisper) este un model pentru
transcrierea vocii dezvoltat de [**OpenAI**](https://openai.com/).

#### Avantaje

- Are o comunitate mare
- Este mereu dezvoltat și actualizat

#### Dezavantaje

- N-are drept logo o broscuță drăguță



### Colectarea datelor


Modelul se va antrena cu ajutorul datelor în format `csv`, similar [structurii utilizate anterior
](#explorarea). Pentru simplitate vom utiliza serviciile oferite de [`HuggingFace`](https://huggingface.co)
pentru gestionarea datelor de antrenare. 

<img alt="treba de colectat" src="https://i.imgflip.com/2xru43.jpg" class="w-1/2"></img> 

Am decis să colectăm date de la voluntari(colegi/prieteni).
Ca să le fie mai simplu voluntarilor să doneze date
am creat un [strîngător de date](https://github.com/Yehoward/iazar-datacollector).

Strîngătorul reprezintă un bot telegram cărui îi transmiți un mesaje vocale și transcrierea lor.

![Strîngătorul ](/img/iazar/colector_help.jpg) 

Dupa o scurtă perioadă de timp am strîns aproximativ 50 de date audio.
Am considerat că este destul pentru o mică adjustare.

Datele colectate au fost plasate pe un depozit de pe [HuggingFace](https://huggingface.co).
Din frica ca să nu fie datele colectate utilizate împotriva voluntarilor,
am decis să facem depozitul privat.


<!-- <img src="https://media.makeameme.org/created/data-data-collection.jpg" alt="TODATA" class="w-2/3"></img> -->



### Common Voice <img alt="mozilla" src="/img/mozilla.webp" class="inline w-16 m-0"></img>

<img alt="here we go again" src="/img/iazar/here-we-go-again.png"></img>
Pentru rezultate mai bune am folosit și date de la [Mozilla Common
Voice](https://commonvoice.mozilla.org/ro/datasets). Anume datele de pe
repozitoriul [HuggingFace Common Voice
11\_0](https://huggingface.co/datasets/mozilla-foundation/common_voice_11_0).



### Codul
<!-- colab, hugging face und wasnot  -->
![despre date](https://interviewquery-cms-images.s3-us-west-1.amazonaws.com/7fec735f-3c5f-49d0-8bac-7a7af954774e.jpg) 

Modelul a fost antrenat pe o versiune gratisă de `GoogleColab`.
Noi am modificat [caietul lui Sanchi Gandhi
](https://colab.research.google.com/github/sanchit-gandhi/notebooks/blob/main/fine_tune_whisper.ipynb#scrollTo=BRdrdFIeU78w)
pentru a satisface necesitățile noastre.
Cele mai notabile modificări fiind:

- Am schimbat bazele de date.

Inițial am antrenat modelul pe bazele de date de la `Common Voice`, apoi am
antrenate pe datele noastre.

- Am schimbat limba, exemplul inițial fiind pentru limba Hindu.

- Am conectat colabul la [`depozitul nostru HuggingFace`](https://huggingface.co/Yehoward/whisper-small-ro)

- Am tradus documentația

Caietul final -> https://nbviewer.org/github/Yehoward/IAZAR/blob/master/code_de_antrenare_iazar.ipynb



### Rezultate

<img src="/img/apu-strong.webp">

Am testat mai multe modele pe un audio înregistrat de către **Crinela**.

<audio controls>
  <source src="/img/iazar/tg-2024-05-04T21:48:19.wav" type="audio/wav">
  Your browser doesn't support HTML5 audio.
</audio>

Textul așteptat:
>
> Mâine este paștele, toți au pregătit păscuțele pentru sfințit.
>

<img src="/img/iazar/evaluare-toate.png">


- Primul îi modelul inițial whisper
- Al doilea îi antrenat numai cu date de la mozilla
- Al treilea îi antrenat numai cu datele noastre
- Al ultimulea îi antrenat atît cu datele noastre cît și cu cele de la
mozila

După cum observăm toate modelel adjustate de noi au performanță mai bună decît
modelul inițial de la `Whisper`. Din punctul nostru de vedere, din toate
modelele cea mai bună performanță a avut-o cea antrenată pe datele noaste și
cele de la Common Voice.

### Metode de implementare a modelului


#### Bot pentru telegram

Am făcut un bot telegram, care poate transcrie înregistrări vocale, fișiere
audio, video pînă la 20Mb (din cauza limitărilor de la telegram).

<img src="/img/iazar/bot_telegram_exemplu.jpg" alt="exemplu" class="w-2/3">

Botul la bază folosește librăria [transformers](https://huggingface.co/docs/transformers/index),
care permite să lucrezi mai ușor cu modele încărcate direct pe `HuggingFace`.

[Sursa cod la robot](https://github.com/Yehoward/iazar-tg-bot)




### Membrii Echipei


<div class="flex flex-row gap-4">
<div class="text-center"> 
<img src="/img/iazar/membrii/eu.webp" class="h-80 w-fit object-contain">
<div class="mt-[-3rem]">
<strong>Yehoward Rudenco</strong>
<br><span class="text-stone-400 italic">Copiist</span>
</div>
</div>

<div class="text-center">
<img src="/img/iazar/membrii/diana.jpg" class="h-80 w-fit object-contain">
<div class="mt-[-3rem]">
<strong>Diana Josan</strong>
<br><span class="text-stone-400 italic">Conducătoare</span>
</div>
</div>

<div class="text-center">
<img src="/img/iazar/membrii/crina.jpg" class="h-80 w-fit object-contain">
<div class="mt-[-3rem]">
<strong>
Crinela Cernelea
</strong>
<br><span class="text-stone-400 italic">Fața proiectului</span>
</div>
</div>


</div>

## Concluzie

<img src="/img/heavy-breath.webp" alt="Conclusion">


<img src="/img/iazar/locul-2.png" alt="Locul 2">

<div class="text-center text-3xl"><strong>Locul 2!!!</strong></div>

Decernarea cîștigătorilor vedeți aici -> [premierea](https://www.facebook.com/watch/?v=451507837601458 ) 
