---
title: "Turbine Eoliene"
description: "Proiect Geografie: vîntu bon'k; turbina vjuh"
pubDate: "26 february 2024"
heroImage: "/src/assets/coperte/turbine.png"
includeMatex: true
---

<script type="module" type="text/javascript">
// const kex = request('katex');

class MathTEX extends HTMLElement {
        connectedCallback(){
                this.innerHTML = katex.renderToString(this.innerText);
            }
    }


customElements.define("ma-tex", MathTEX);


</script>


## Ce este o turbină eoliană?


Turbina eoliană este o instalație ce transformă energia cinetică a vîntului în energie electrică.
Vîntul pune în mișcare elicile turbinei, care fiind conectate la generator, produce energie electrică.

Simplu, nu?

Aceasta generalizare ne v-a ajuta ulterior să înțelegem mai bine natura turbinelor eoliene.

![Turbina eoliana](/img/turbine/turbina-orizontala.jpg) 



## Putere

![putere](/img/turbine/putere.jpg) 


Acum știm că turbina transformă energia vîntului în cea electrică, deci calculînd
energia vîntului putem calcula cantitatea de electricitate generată de turbină.
Noi știm că energia generată nu poate depăși energia vîntului,
aceasta ar însemna că energia apare din nimic, ceea ce violează
[legile conservării
energiei](https://wiki.froth.zone/wiki/Legea_conserv%C4%83rii_energiei?lang=ro).(Legile date nu sunt ca niște promisiune spuse de mine. Sunt legi cărora li se supune tot ce există.)

Energia(cinetică) a vîntului poate fi calculată cu relația  <ma-tex>E\_c=\frac{m\cdot v^2}{2}</ma-tex>,
unde __m__ este masa aerului care antrenează elicile turbinei, iar **v** este viteza masei de aer.
Viteza medie anuală a vîntului în Briceni este de <ma-tex>2 m/s</ma-tex> [^date_vint].
Masa de aer poate fi calculată dacă îmi vom imagina aerul ca un cilindru, baza fiind cercul descris de elicea efectînd o revoluție,
iar înalțimea fiind distanța parcursă de vînt într-un timp **t** dat, exemplificat în imaginea de mai jos.

![energia cinetica](/img/turbine/calcularea-energiei.png) 

Suprafața lovită de aer v-a fi:

<ma-tex>
\begin{aligned}
S&=\pi\cdot r^2
\end{aligned}
</ma-tex>

Pentru simplitate vom folosi în calcule o turbină cu raza <ma-tex>r=1m</ma-tex>.

Volumul de aer care v-a lovi turbina, se v-a calcula dupa relația de calcularea volumului unui cilindru:

<ma-tex>
\begin{aligned}
V&=S\cdot d
\end{aligned}
</ma-tex>

Unde distanța este <ma-tex> d = v \cdot t</ma-tex>:
<!-- adică volumul de aer ce va trece prin turbină cu viteza de <ma-tex>2m/s</ma-tex> timp de <ma-tex>1s</ma-tex>. -->

<ma-tex>
\begin{aligned}
V&=\pi \cdot r^2 \cdot v \cdot t
\end{aligned}
</ma-tex>

Masa va fi produsul dintre volumul și densitatea aerului (<ma-tex>\rho=1.225 \frac{kg}{m^3}</ma-tex>)

<ma-tex>
\begin{aligned}
m&=V\cdot \rho\\
m&=\pi \cdot r^2 \cdot v \cdot t \cdot \rho
\end{aligned}
</ma-tex>

Din acestea deducem relația:

<ma-tex class="text-2xl">E\_c=\frac{\pi \cdot r^2 \cdot t \cdot \rho \cdot v^3}{2}</ma-tex>

Unde pentru simplitate <ma-tex>t = 1s</ma-tex>

<ma-tex class="text-4xl inline-block w-full text-center">E\_c=\frac{\pi \cdot r^2 \cdot \rho \cdot v^3}{2}</ma-tex>


Drept într-o lume perfectă, unde nu-s de loc pierderi de energie și unde eu finisez la timp proiectele,
o turbină eoliană cu elicea de <ma-tex>1m</ma-tex> și la viteza de <ma-tex>2m/s</ma-tex> v-a produce <ma-tex>~15,4J</ma-tex> timp de o secundă, sau <ma-tex>~15.4W</ma-tex>. 

| Dispozitiv | Putere(W) |
|-|-|
| Bec incandescent | 60-100 W|
| Bec LED |   &pm;10-20W |
| Fierbător de apă | 1200W |
| TV LED 0.5m | 15W |
[^puterea_pd]


Aceasta putere este destulă pentru a porni o lumină economă.

Desigur în realitate acea turbină de **1m** în aceleași condiții v-a produce mai puțină energie.
Acesta se datorează faptului că vîntul nu-și cedeaza toata energia ce se explică prin faptul că moleculele de aer,
comparativ cu păsările, nu sunt complet oprite de turbină.

![Betz](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Albert_Betz.jpg/220px-Albert_Betz.jpg) 
Aceste pierderi au fost calculate de catre savantul **Albert Betz**[^a_Betz], care a constatat că nici o turbină eoliană 
indiferent de formă nu v-a putea captura mai mult de <ma-tex>59.3\\%</ma-tex> din energia cinetică a vîntului.
Drept <ma-tex>15.4W \cdot  59.3 \\% = 9,1W</ma-tex>. La moment turbinele eoliene ating pînă la <ma-tex>80\\%</ma-tex> din limita lui Betz. [^betz]

## Forma 

În general sunt 2 tipuri de turbine eoliene:

### Axă Orizontală

![turbine orizontale](/img/turbine/turbine_o_mid.jpg) 

Mecanismul de rotație este poziționat orizontal. Este tip cel mai des întîlnit. Și dispune de o eficiență foarte înaltă,
care variază între **30-50%**.[^eficienta-turb]

Avantaje:

+ Mai fiabile
+ Sunt Mai eficiente
<!-- + Poate lucra într-un interval mai mare de vitezi de vînt [^eficienta-or] -->

### Axă Verticală

![verticale](https://searx.be/image_proxy?url=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F6%2F6f%2FVertical_Axis_Wind_Turbines.png%2F320px-Vertical_Axis_Wind_Turbines.png&h=ac3b9ce5559320b7370ff31d778c62127b5b696ffafa6cbfb5bc7d732fd15a5b) 


Mecanismul de rotație a turbinelor date este poziționat vertical. Acest tip turbine este întîlnit mai rar.
Ele sunt folosite pentru producerea energiei în cantități mai mici în locuri limitate de spațiu.[^eficienta-turb]

Avantaje:

+ N-are nevoie de redirecționare în caz că vîntul își schimbă direcția
+ Mecanismul se află la nivel cu pămîntul, drept sunt mai ușor acesate
+ Mai puțin periculoase pentru păsări



Sunt mai multe tipuri de turbine pe axă verticală. Des întîlnite sunt **Darrius** și **Savonius**.


#### Darrius 

![mixer](https://imgs.search.brave.com/UGzgi_pPuLVRuZ-ErQS0OGjv-F8H0ZjXI7SlLJbZh9w/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9lbi53/aW5kLXR1cmJpbmUt/bW9kZWxzLmNvbS9n/ZXRmb3RvLWxSeXRK/ZWJDYkRQLXR1cmJp/bmUtZG9ybmllcl9k/YXJyaWV1cy01NS0x/OTY3OS5qcGc) 

Eficienta turbinelor Darrius variază de la **30-40%**[^eficienta-turb]. 
Cu toate că au o eficiență destul de înaltă, sunt mai puțin fiabile[^sigur] și deseori n-au capacitatea de autopornire. 

#### Savonius

![helix](https://searx.be/image_proxy?url=https%3A%2F%2Fencrypted-tbn0.gstatic.com%2Fimages%3Fq%3Dtbn%3AANd9GcRoBxgC5SUQHSnz-yZ5_QGZNDhhZqVTMXQg77xDI63WHBVBOh4Y%26s&h=5bc71ff5748f1525485d51a2a21a6acb49bca5c42fb5d9bf505fb5369bd1c1ea) 

<p style="display: flex;">
<img src="https://searx.be/image_proxy?url=https%3A%2F%2Fi.pinimg.com%2F236x%2F25%2F5b%2F4f%2F255b4f4c7561f488039906b8b09d579b.jpg&h=792e795b4ebdfdc5aa214a04aca302401c2f524f5755ca8e4aed14952d14c366" alt="savonius" > <img src="https://searx.be/image_proxy?url=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fd%2Fd4%2FSavonius_fluid_flow.gif%2F207px-Savonius_fluid_flow.gif&h=b5ab09c4887a7fe4ac0180b14f7f18e9e0b508c7dde99f4b44c3b62ca8f9550c" alt="savonius-scheme">

</p>

Savonius este o turbina cu eficiență mai scăzută, la un interval de **10-17%**.

Farmecul turbinelor Savonius este structura lor simplă și fiabilitatea lor[^eficienta-turb].


### Concluzie

![concluzie](/img/turbine/ponyatno.png) 

---
[^zgomot de la turbine]: pdf
[^puterea_pd]: [puterea pentru diferite dispozitive](https://www.daftlogic.com/information-appliance-power-consumption.html)
[^date_vint]: [date de vint](https://statbank.statistica.md/PxWeb/pxweb/ro/10%20Mediul%20inconjurator/10%20Mediul%20inconjurator__MED010/MED010300reg.px/table/tableViewLayout2/?rxid=9909cfe9-5251-4195-b9df-46618fac817f)
[^motoare]: [Motoare generatoare](https://yewtu.be/watch?v=WabSvtKPjlk)
[^betz]: [legea lui Betz](https://en.wikipedia.org/wiki/Betz%27s_law)
[^a_Betz]: [Albert Betz](https://en.wikipedia.org/wiki/Albert_Betz)
[^eficienta-turb]: [Eficienta turbinelor eoliene](https://lambdageeks.com/wind-turbine-efficiency/)
[^eficienta-or]: [Eficienta turbinelor orizontale](https://www.energyabcs.com/advantages-and-disadvantages-of-horizontal-axis-wind-turbine/)
[^sigur]: fiabil - Care prezintă siguranță în funcționare

