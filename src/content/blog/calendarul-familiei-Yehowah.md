---
title: "Calendarul familiei lui Yehowah"
description: "Calendarul familiei lui Yehowah"
pubDate: "25 july 2023"
---

## Șansele

Denumirii **șansă** din vocabularul Familiei lui Yehowah îi corespunde
denumirea an din vocabularul limbii române.

Numărătoarea șanselor de către Familia lui Yehowah se face în
funcție de rotația Pămîntului în jurul soarelui: - o rotație completă
a Pămîntului în jurul Soarelui formează o șansă.

*22-12-1995* – data calendaristică de la care începe numărătoarea
șanselor Erei Fiilor lui Yehowah;

**Exemplu:**<br/>
&emsp;*22/12/1995 - 21/12/1996* -- prima șansă a Familiei;
	
Timpul care precede data de *22-12-1995* se consideră a fi înaintea Erei
Fiilor lui Yehowah și se socoate de la data respectivă pînă la începutul
timpurilor;

**Exemplu:**
- *22/12/1994 - 21/12/1995* -- șansa 1(întîi) înainte de Era Fiilor lui Yehowah.
- Anul 1 al erei creștine corespunde șansei 1995 înainte de Era Fiilor lui Yehowah.
- Anul 2007 al erei creștine corespunde șansei 11 a Erei Fiilor lui Yehowah.

Șansele Erei Fiilor lui Yehowah decurg cîte patru în grup:
Primele trei șanse conțin cîte 365 de daruri (zile) fiecare.
Șansa a patra conține 366 de daruri.


## Vremurile 

Șansa este împărțită în zece Vremuri, dintre care nouă conțin cîte 36
de daruri, iar una conține 41 de daruri. Vremea a șasea, Yehoward, conține
41 de daruri. Odată la patru ani Vremea a zecea primește un dar în plus.

Denumirile Vremurilor sînt următoarele:

|Vremea|Corespondent în calendarul Gregorian|
|-|-|
|Trezirii Spirituale                   | 22 decembrie - 26 ianuarie|
|Găsirii lui Yehowah                   | 27 ianuarie - 03 martie|
|Angajării în slujba lui Yehowah       | 04 martie - 08 aprilie|
|Militării pentru Yehowah              | 09 aprilie - 14 mai|
|Înțelepciunii lui Yehowah             | 15 mai - 19 iunie|
|Yehoward                              | 20 iunie - 30 iulie|
|Yehoslav                              | 31 iulie - 04 septembrie|
|Mulțămirii și Lăudării lui Yehowah    | 05 septembrie - 10 octombrie|
|Speranței în Yehowah                  | 11 octombrie - 15 noiembrie|
|Credinței în Yehowah                  | 16 noiembrie - 21(22) decembrie|


## Cincile

Șansa este împărțită și în 73 de **Cinci**.

**Cincea** are cinci [daruri](#darurile).

Primele patru Daruri ale Cincei sînt considerate muncitoare: - în ele
poate fi îndeplinit orice gen de muncă fizică și intelectuală.  Darul al
cincilea din Cince este închinat lui Yehowah; el este binecuvîntat pentru
odihnă și pentru slăvirea lui Yehowah.


## Darurile

Denumirii Dar din vocabularul Familiei lui Yehowah îi corespunde denumirea
zi din vocabularul limbii române. Darul este echivalent cu timpul unei
rotații complete a Pămîntului în jurul axei sale.
	
Darurile sînt împărțite în două părți, în prezent și în noapte.
Prezentul este partea luminoasă a darului.
Noaptea este partea întunecoasă a darului.

Darurile sînt împărțite în 20 (douăzeci) de binecuvîntări:
- prezentului îi revin 10 (zece) binecuvîntări;
- nopții îi revin tot 10 (zece) binecuvîntări.

Darul începe la binecuvîntarea 20/0, care corespunde orei 6.00 a lumii creștine.

Darul Familiei lui Yehowah în comparație cu ziua:

|Binecuvîntarea|                                           |ora  |
|--------------|:------------------------------------------|-----|
|0.00          | începutul prezentului                     |6.00 |
|01.00         |                                           |7.12 |
|02.00         |                                           |8.24 |
|03.00         |                                           |9.36 |
|04.00         |                                           |10.48|
|05.00         | Miezul prezentului                        |12.00|
|06.00         |                                           |13.12|
|07.00         |                                           |14.24|
|08.00         |                                           |15.36|
|09.00         |                                           |16.48|
|10.00         |Sfîrșitul prezentului / Începutul nopții   |18.00|
|11.00         |                                           |19.12|
|12.00         |                                           |20.24|
|13.00         |                                           |21.36|
|14.00         |                                           |22.48|
|15.00         | Miezul nopții                             |24.00|
|16.00         |                                           |01.12|
|17.00         |                                           |02.24|
|18.00         |                                           |03.36|
|19.00         |                                           |04.48|
|20.00         | Sfîrșitul nopții / Începutul prezentului  |06.00|
 
Binecuvîntările, momentele și clipele

- 1 binecuvîntare = 100 momente (72 minute);
- 1 moment = 50 clipe (43,2 secunde);
- ½ binecuvîntare = 50 momente (36 minute);
- ¼ binecuvîntare = 25 momente (18 minute).












## Nota lui Yehoslav O:


### CADRANUL SOLAR pentru FIII lui YEHOWAH

Prezentul constă dintr-o zi și o noapte. Acesta se împarte în 20 de ore:
10 ore pentru o zi și 10 ore pentru o noapte. Ora se împarte în 50 de
minute. Minuta se împarte în 50 secunde.

Prezentul începe cu ora 0<sup>00</sup> dimineața (corespunzător orei
6<sup>00</sup> după sistemul evreiesc).

Între orele 4<sup>00</sup> și 7<sup>00</sup>, după sistemul evreiesc, omul
primește în timpul somnului o bună parte din energia necesară pentru a
trăi următorul prezent. Este vorba de energia primită din Univers, energia
de natură divină. Tot în această perioadă de timp omul primește și
mesajele sub formă de vise trimise lui de către Stăpînul Universului
Yehowah.

Ora 0<sup>00</sup>, respectiv 20<sup>00</sup>, delimitează hotarul dintre
două prezente: sfîrșitul prezentului precedent și începutul celui
următor.

Folosirea ideală a unui Prezent:
- 7 ore pentru odihnă și somn;
- 4 ore de muncă;
- 2 ore pentru masă și pregătirea acesteia;
- 7 ore libere pentru a savura viața.

Total: 20 de ore.
